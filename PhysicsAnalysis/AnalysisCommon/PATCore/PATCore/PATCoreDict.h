/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

// Dear emacs, this is -*-c++-*-

#ifndef PATCORE_PATCOREDICT_H
#define PATCORE_PATCOREDICT_H

/**
   @brief For dictionary generation.

   @author Karsten Koeneke (CERN)
   @date   October 2011

*/

#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#include "PATCore/AcceptInfo.h"
#include "PATCore/AcceptData.h"
#include "PATCore/TSelectorToolBase.h"
#include "PATCore/IAsgSelectionTool.h"

#ifndef ROOTCORE
#include "PATCore/IAthHistogramTool.h"
#include "PATCore/IUserDataCalcTool.h"
#endif

#endif
